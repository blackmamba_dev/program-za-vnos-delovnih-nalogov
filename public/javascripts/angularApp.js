angular.module('delovniNalogi', ['ui.router'])
// ui.router config
.config([           							
	'$stateProvider',
	'$urlRouterProvider',
	function($stateProvider, $urlRouterProvider) {
				
		$stateProvider
			.state('home', {
				url: '/home',
				templateUrl: '/home.html',
				controller: 'MainCtrl',
				resolve: {
					postPromise: ['posts', function(posts) {
						return posts.getAll();
					}]
				}
			})
			
			.state('posts', {
				url: '/posts/{id}',
				templateUrl: '/posts.html',
				controller: 'PostsCtrl',
				resolve: {
					post: ['$stateParams', 'posts', function($stateParams,posts) {
						return posts.get($stateParams.id);
					}]
				}
			})

			.state('osebe', {
				url: '/osebe',
				templateUrl: '/osebe.html',
				controller: 'MainCtrl',
				resolve: {
					postPromise: ['posts', function(posts) {
						return posts.getAll();
					}]
				}
			})

			.state('stranke', {
				url: '/stranke',
				templateUrl: '/stranke.html',
				controller: 'MainCtrl',
				resolve: {
					postPromise: ['posts', function(posts) {
						return posts.getAll();
					}]
				}
			});

		$urlRouterProvider.otherwise('home');
	}
])

// Main controller
.controller('MainCtrl', [
'$scope',
'posts',
function($scope, posts){
	
	$scope.posts = posts.posts;
	
	$scope.addPost = function() {
		if ($scope.title === '') { return; }
		posts.create({
			oseba: $scope.oseba,
			datum_naloge: $scope.datum_naloge,
			datum_zakljucka: $scope.datum_zakljucka,
			stranka: $scope.stranka,
			st_ur: $scope.st_ur,
		});
		$scope.oseba = '';
		$scope.datum_naloge = '';
		$scope.datum_zakljucka = new Date();
		$scope.stranka = '';
		$scope.st_ur = '';

	};
	
	$scope.deletePost = function(post) {
		posts.delete(post);
	}
	
}])

// Post controller
.controller('PostsCtrl', [
'$scope',
'posts',
'post',
function($scope, posts, post) {
	$scope.post = post;
	
}])

// Angular service/factory
.factory('posts', ['$http', function($http){
	// service body
	var o = {
		posts: []
	};
	// get all posts
	o.getAll = function() {
		return $http.get('/posts').success(function(data) {
			angular.copy(data, o.posts);
		});
	};
	// create new posts
	o.create = function(post) {
		return $http.post('/posts', post).success(function(data) {
			o.posts.push(data);
		});
	};
	// get single post
	o.get = function(id) {
		return $http.get('/posts/' + id).then(function(res) {
			return res.data;
		});
	};
	// delete single post
	o.delete = function(post) {
		return $http.delete('/posts/' + post._id).success(function(data) {
			angular.copy(data, o.posts);
		});
	}
	
	return o;
}])