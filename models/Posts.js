var mongoose = require('mongoose');

var PostSchema = new mongoose.Schema({
	oseba: String,
	datum_naloge: { type: Date, default: Date.now },
	datum_zakljucka: Date,
	stranka: String,
	st_ur: Number,
});

mongoose.model('Post', PostSchema);